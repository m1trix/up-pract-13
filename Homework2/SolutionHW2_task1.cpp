/*
    Задача 1:
    Условие:
        Напишете програма, която въвежда от стандартния вход реално число, символ и второ реално число.
        След това разпознава въведения знак и извършва съответното математическо действие, което  се реализира чрез използването на функции. По една за всяка операция. Допустими знаци са '+', '-', '*', '/'.
        Ако  се въведен друг знак, трябва да се изведе съобщение за грешка.
        Резултатът се извежда на екрана в подходящ формат.
        Деление на 0 не се допуска.
        Примерен вход и изход:

    Input expression: 23 + 45
     = 68

    Пример:
    Input expression: 100.5 / 0
    Division by zero!
*/

/*
    Критерии за оценяване:
    3 - Използвани са функции за всяка операция и за обработка на израза.
    1 - Въвеждане на двете числа и символа.
    3 - Откриване вида на операцията и връщане на пресметнатия резултат.
    2 - Проверка за деление на нула.
    1 - Проверка за непознат символ.
*/

#include <iostream>
using namespace std;


void calcExpression(double left, double right, char sign);
double addition (double left, double right);
double subtraction (double left, double right);
double multiplication (double left, double right);
double division (double left, double right);

int main() {
    // ========================
    // INPUTING THE EXPRESSION:
    // ========================
    double left = 0, right = 0;
    char sign;

    cout << "Input the expression: ";
    cin >> left >> sign >> right;

    // ======================
    // DISPLAYING THE ANSWER:
    // ======================
    calcExpression(left,right,sign);
    cout << endl;

    return 0;
}

void calcExpression(double left, double right, char sign){
    switch(sign) {
        case '+':
            cout << " = " << left + right;
            break;

        case '-':
            cout << " = " << left - right;
            break;

        case '*':
            cout << " = " << left * right;
            break;

        case '/':
            if(0 != right)
                cout << " = " << left / right;
            else
                cout << "Division by zero!";
            break;

        default:
            cout << "Unknown operation!";
    }
}

double addition (double left, double right){
    return left + right;
}

double subtraction (double left, double right){
    return left - right;
}

double multiplication (double left, double right){
    return left * right;
}

double division (double left, double right){
    return left / right;
}
/*   Тест 1:
        Вход:
            5 / 0
        Изход:
            Division by zero!

    Тест 2:
        Вход:
            3 @ 4
        Изход:
            Unknown operation!

    Тест 3:
        Вход:
            3.3 * 3.3
        Изход:
             = 10.89
*/

