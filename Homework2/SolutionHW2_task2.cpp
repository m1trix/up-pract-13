//Задача 2
/*  Условие:
    Да се напише програма, в която потребителя въвежда целочислен масив с размер n, 0 < n <= 100.
    Потребителя въвежда n от клавиатурата. От масива трябва да се “премахнат” всички елементи,
    които не са прости числа.
    Под “премахнат”, се има в предвид да се преместят в края на масива и размерът му - n, да се намали,
    така че от 0 до n-1 да има само прости числа. Ако няма прости числа, размерът му става 0.
    Не се изисква стойностите на “премахнатите” елементи да се запазят, ако смятате,
    че е нужно, може да ги унищожите в процеса на “премахване”.

    Примерен вход и изход:

    Input the size [1,100]: 5
    Input the array:
    array[0] = 1
    array[1] = 2
    array[2] = 3
    array[3] = 4
    array[4] = 5

    Array: 2 3 5

*/

/*
    Критерии за оценяване:

    1 - Използване на статичен масив с размер 100.
    1 - Въвеждане на размер на масива - n.
    2 - Въвеждане на стойности на елементите.
    2 - Правилна проверка за прости числа.
    3 - Преместване на ненужните числа в края.
    1 - Коректна промяна на размера на масива - k.
*/

#include <iostream>
using namespace std;

const int MAX_SIZE = 100;

int inputSize();
bool isPrime(int);
void inputArray  (int[], int);
void printArray  (int[], int);
int removeNumbers(int[], int);
void makeZeros   (int[], int);
int moveZeros    (int[], int);

int main() {

    int n = inputSize();
    int array[MAX_SIZE];
    inputArray(array, n);

    n = removeNumbers(array, n);
    printArray(array, n);

    return 0;
}

int inputSize() {
    int size = 0;
    while(size < 1 || size > MAX_SIZE) {
        cout << "Input the size [1," << MAX_SIZE << "]: ";
        cin >> size;
    }

    return size;
}

void inputArray(int array[], int size) {
    cout << "Input the array: " << endl;
    for(int i = 0; i < size; ++i) {
        cout << "array[" << i << "] = ";
        cin >> array[i];
    }

    cout << endl;
}

void printArray(int array[], int size) {
    cout << "Array: ";
    for(int i = 0; i < size; ++i)
        cout << array[i] << " ";
    cout << endl;
}

int removeNumbers(int array[], int size) {

    makeZeros(array, size);

    return moveZeros(array, size);
}

bool isPrime(int number) {
    if(number <= 1)
        return false;

    for(int divider = 2; divider < number; ++divider) {
        if(number % divider == 0)
            return false;
    }

    return true;
}

void makeZeros(int array[], int size) {
    for(int i = 0; i < size; ++i) {
        if(false == isPrime(array[i]))
            array[i] = 0;
    }
}

int moveZeros(int array[], int size) {
    int index = 0;
    for(int i = 0; i < size; ++i) {
        if(array[i] != 0) {
            array[index] = array[i];
            ++index;
        }
    }

    return index;
}

/*  Тест 1:
        Input the size [1,100]: 5
        Input the array:
        array[0] = 1
        array[1] = 2
        array[2] = 3
        array[3] = 4
        array[4] = 5
    Expected:
    Array: 2 3 5


    Тест 2:
        Input the size [1,100]: 10
        Input the array:
        array[0] = 2
        array[1] = 4
        array[2] = 5
        array[3] = 6
        array[4] = 12
        array[5] = 23
        array[6] = 60
        array[7] = 97
        array[8] = 103
        array[9] =
    Expected:
    Array: 2 5 23 97 103


    Тест 3:
        Input the size [1,100]: 5
        Input the array:
        array[0] = 1
        array[1] = 10
        array[2] = 100
        array[3] = 10000
        array[4] = 100000
    Expected:
    Array:

    Тест 4:
        Input the size [1,100]: 3
        Input the array:
        array[0] = -2
        array[1] = -5
        array[2] = 7
    Expected:
    Array: 7

*/
