/*
    Задача 1:
    Условие:
    Да се напише програма, в която потребителя въвежда координатите на три точки в равнината.
    Програмата трябва да изчисли дължината на трите страни, периметъра и лицето на триъгълника,
    образуван от въведените три точки. Резултатите да се изведат на екрана в разбираем и ясен вид.

*/
/*
    Пример:
    Point 0 x: 0
    Point 0 y: 0

    Point 1 x: 0
    Point 1 y: 1

    Point 2 x: 1
    Point 2 y: 0

    RESULTS:
    Side 0 = 1
    Side 1 = 1
    Side 2 = 1.41421
    P = 3.41421
    S = 0.5
*/

/*
    Критерии за оценяване:
    1 - Кодът се компилира успешно.
    1 - Въвеждане на координатите на точките.
    2 - Изчисляване на размерите на страните.
    2 - Изчисляване на периметъра на триъгълника.
    2 - Изчисляване на лицето на триъгълника.
    1 - Извеждане на размера на страните на екрана.
    1 - Извеждане на периметъра и лицето на екрана.
*/

#include <iostream>
#include <math.h>
using namespace std;


const int POINTS_COUNT = 3;


double pointDistance(double, double, double, double);
double perimeter(double, double, double);
double area(double, double, double);

int main() {
    // ====================
    // INPUTING THE POINTS:
    // ====================
    double pointsX[POINTS_COUNT];
    double pointsY[POINTS_COUNT];

    for(int i = 0; i < POINTS_COUNT; ++i) {
        cout << "Point" << i << " x = ";
        cin >> pointsX[i];
        cout << "Point" << i << " y = ";
        cin >> pointsY[i];
        cout << endl;
    }

    // ===========================
    // GETTING THE TRIANGLE SIDES:
    // ===========================
    double sides[POINTS_COUNT];
    for(int i = 0; i < POINTS_COUNT; ++i) {
        sides[i] = pointDistance(pointsX[i % POINTS_COUNT], pointsY[i % POINTS_COUNT],
                                 pointsX[(i+1) % POINTS_COUNT], pointsY[(i+1) % POINTS_COUNT]);
    }

    // =====================
    // PRINTING THE RESULTS:
    // =====================
    cout << "RESULTS:" << endl;
    for(int i = 0; i < POINTS_COUNT; ++i)
        cout << "Side " << i << " = " << sides[i] << endl;

    cout << "P = " << perimeter(sides[0], sides[1], sides[2]) << endl;
    cout << "S = " << area(sides[0], sides[1], sides[2]) << endl;

    return 0;
}


double pointDistance(double px1, double py1, double px2, double py2) {
    return sqrt((px1 - px2)*(px1 - px2) + (py1 - py2)*(py1 - py2));
}

double perimeter(double a, double b, double c) {
    return a + b + c;
}

double area(double a, double b, double c) {
    double p = perimeter(a, b, c) / 2;

    return sqrt(p*(p-a)*(p-b)*(p-c));
}

/*
    Тест 1:
        Вход:
            0 0 1 1 0 1
        Изход:
            Side 0 = 1.41421
            Side 1 = 1
            Side 2 = 1
            P = 3.41421
            S = 0.5
*/

