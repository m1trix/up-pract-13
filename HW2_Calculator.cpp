/*
    Условие:
      Напишете програма, която въвежда от стандартния вход реално число, символ и второ реално число.
    След това разпознава въведения знак и извършва съответното математическо действие.
    Резултатът се извежда на екрана. Допустими знаци са '+', '-', '*', '/'.
    Ако е въведен друг знак, трябва да се изведе съобщение за грешка.
    Деление на 0 не се допуска.
*/

/*
    Пример:
    Input expression: 3 + 5
     = 8

    Пример:
    Input expression: 100.5 / 0
    Division by zero!
*/

/*
    Критерии за оценяване:
    1 - Кодът се компилира успешно.
    2 - Въвеждане на двете числа и символа.
    3 - Откриване вида на операцията и връщане на пресметнатия резултат.
    2 - Проверка за деление на нула.
    2 - Проверка за непознат символ.
*/

#include <iostream>
using namespace std;

void calcExpression(double left, double right, char sign){
    switch(sign) {
        case '+':
            cout << " = " << left + right;
            break;

        case '-':
            cout << " = " << left - right;
            break;

        case '*':
            cout << " = " << left * right;
            break;

        case '/':
            if(0 != right)
                cout << " = " << left / right;
            else
                cout << "Division by zero!";
            break;

        default:
            cout << "Unknown operation!";
    }

}

int main() {
    // ========================
    // INPUTING THE EXPRESSION:
    // ========================
    double left = 0, right = 0;
    char sign;

    cout << "Input the expression: ";
    cin >> left >> sign >> right;


    // ======================
    // DISPLAYING THE ANSWER:
    // ======================
    calcExpression(left,right,sign);
    cout << endl;

    return 0;
}

/*
    Тест 1:
        Вход:
            5 / 0
        Изход:
            Division by zero!

    Тест 2:
        Вход:
            3 @ 4
        Изход:
            Unknown operation!

    Тест 3:
        Вход:
            3.3 * 3.3
        Изход:
             = 10.89
*/
