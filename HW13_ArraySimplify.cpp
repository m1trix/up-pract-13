#include <iostream>
using namespace std;

const int MAX_SIZE = 100;

int inputSize();
void inputArray(unsigned[], int);
void printArray(unsigned[], int);
int removeNumbers(unsigned[], int);
bool isPrime(unsigned);
void makeZeros(unsigned[], int);
int moveZeros(unsigned[], int);

int main() {

    int n = inputSize();
    unsigned array[MAX_SIZE];
    inputArray(array, n);

    n = removeNumbers(array, n);
    printArray(array, n);

    return 0;
}

int inputSize() {
    int size = 0;
    while(size < 1 || size > MAX_SIZE) {
        cout << "Input the size [1," << MAX_SIZE << "]: ";
        cin >> size;
    }

    return size;
}

void inputArray(unsigned array[], int size) {
    cout << "Input the array: " << endl;
    for(int i = 0; i < size; ++i) {
        cout << "array[" << i << "] = ";
        cin >> array[i];
    }

    cout << endl;
}

void printArray(unsigned array[], int size) {
    cout << "Array: ";
    for(int i = 0; i < size; ++i)
        cout << array[i] << " ";
    cout << endl;
}

int removeNumbers(unsigned array[], int size) {

    makeZeros(array, size);

    return moveZeros(array, size);
}

bool isPrime(unsigned number) {
    if(number == 0 || number == 1)
        return false;

    int divider = 2;
    for( ; divider < number; ++divider) {
        if(number % divider == 0)
            return false;
    }

    return true;
}

void makeZeros(unsigned array[], int size) {
    for(int i = 0; i < size; ++i) {
        if(false == isPrime(array[i]))
            array[i] = 0;
    }
}

int moveZeros(unsigned array[], int size) {
    int index = 0;
    for(int i = 0; i < size; ++i) {
        if(array[i] != 0) {
            array[index] = array[i];
            ++index;
        }
    }

    return index;
}
