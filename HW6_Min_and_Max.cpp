/*
    Условие:
      Да се напише програма, която получава от потребителя цяло число от 1 до 100 - размер
    на масив от реални числа, както и елементите на масива. Да се намерят и изведат на екрана
    най-големият и най-малък по стойност елемент.
      Направете контрол на входа.
*/
 
/*
    Пример:
    Input array size: 10
    Input the array: 1 2 3 4 5 6 7 8 9 10
    Max = 10
    Min = 1
*/
 
/*
    Критерии за оценяване:
    1 - Кодът се компилира успешно.
    1 - Въвеждане размер на масива.
    1 - Контрол на входа.
    2 - Въвеждане елементите на масива.
    2 - Намиране на максимален елемент.
    2 - Намиране на минимален елемент.
    1 - Извеждане на елементите на екрана.
*/
 
#include <iostream>
using namespace std;
 
const int MIN_ARRAY_SIZE = 1;
const int MAX_ARRAY_SIZE = 100;
 
 
int main() {
    // ========================
    // INPUTING THE ARRAY SIZE:
    // ========================
    int size = 0;
    do {
        cout << "Input array size: ";
        cin >> size;
    } while(size < MIN_ARRAY_SIZE || size > MAX_ARRAY_SIZE);
 
    // ===================
    // INPUTING THE ARRAY:
    // ===================
    double array[MAX_ARRAY_SIZE];
    cout << "Input the array: ";
 
    for(int i = 0; i < size; ++i)
        cin >> array[i];
 
    // ========================================
    // FINDING THE MINIMAL AND MAXIMAL ELEMENT:
    // ========================================
    double min = array[0], max = min;
    for(int i = 1; i < size; ++i) {
        if(array[i] < min)
            min = array[i];
        else if(array[i] > max)
            max = array[i];
    }
 
    cout << "Max = " << max << endl;
    cout << "Min = " << min << endl;
 
    return 0;
}