/*
    Условие:
      Да се напише програма, която получава от потребителя 2 символни низа с дължина,
    не повече от 256 символа. На екрана трябва да се изведе с число, колко пъти вторият
    низ, изцяло се съдържа в първия.
*/

/*
    Пример:
    Input text: I work late tonight, that's why I'm sad... I hate FMI!
    Input word: I
    4 times.
*/

/*
    Критерии за оценяване:
    1 - Кодът се компилира.
    2 - Въвеждане на низовете.
    6 - Намиране броя ан думите.
    1 - Извеждане резултат на екрана
*/

#include <iostream>
#include <string.h>
using namespace std;



const int MAX_TEXT_SIZE = 256;



int findWord(const char[], const char[]);

int main() {

	// ==============
	// INPUTING TEXT:
	// ==============
	char text[MAX_TEXT_SIZE];
	cout << "Input text: ";
	cin.getline(text, MAX_TEXT_SIZE);

	// ==============
	// INPUTING WORD:
	// ==============
	char word[MAX_TEXT_SIZE];
	cout << "Input word: ";
	cin.getline(word, MAX_TEXT_SIZE);

	// =====================================
	// GETTING THE RESULT AND DISPLAYING IT:
	// =====================================
	int wordFrequency = findWord(text, word);

	cout << wordFrequency << " times." << endl;

	return 0;
}

int findWord(const char text[], const char word[]) {
    int wordSize = strlen(word);
    int count = 0;

    for(int i = 0; text[i] != '\0'; ++i)
        if(text[i] == word[0])
            if(0 == strncmp(text + i, word, wordSize))
                ++count;

    return count;
}

/*
    Тест 1:
        Вход:
            !@#$%^&*()(*&^%$#$%^&*()_)(*&^%$#$%^&*()_+_)(*&^%$#$%^&*()_
            (
        Изход:
            7 times.
*/
