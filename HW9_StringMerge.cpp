/*
    Условие:
        Напишете програа, в която потребителят въвежда 2 символни низа с дължина по-малко от 100.
    Програмата трябва да задели динамична памет за всеки от двата низа и да ги копира.
    Не трябва да има излишна памет.
        Да се напише функция, която получава като аргументи два символни низа и връща
    нов, динамично заделен низ, съдържащ двата подадени, като левият аргумент е пръв.
    Отново, не трябва да има излишна памет.
        Да се изведе на екрана новосъздадения низ.
*/

/*
    Пример:
    Input first: I want
    Input second:  to learn c++
    Result: I want to learn c++
*/

/*
    Критерии за оценяване:
    1 - Кодът се компилира успешно.
    2 - Заделяне на динамична памет за двата входни низа.
    1 - Въвеждане на първият низ.
    1 - Въвеждане на вторият низ.
    1 - Заделяне на динамична памет за изходния низ.
    2 - Копиране на входните низове във изходния.
    1 - Изведане на изходния низ върху екрана.
    1 - Изтриване на динамична памет.
*/

#include <iostream>
#include <string.h>
using namespace std;


const int BUFFER_SIZE = 100;


char* copyString(const char*);
char* mergeStrings(const char*, const char*);


int main() {
    char buffer[BUFFER_SIZE];

    // =============
    // FIRST STRING:
    // =============
    cout << "Input the first string: ";
    cin.getline(buffer, BUFFER_SIZE);
    char* firstString = copyString(buffer);

    // ==============
    // SECOND STRING:
    // ==============
    cout << "Input the second string: ";
    cin.getline(buffer, BUFFER_SIZE);
    char* secondString;
    secondString = copyString(buffer);

    // ==============
    // MERGE STRINGS:
    // ==============
    char* mergedString = mergeStrings(firstString, secondString);
    delete[] firstString;
    delete[] secondString;

    cout <<"Result: " << mergedString << endl;
    delete[] mergedString;

    return 0;
}

char* copyString(const char* source) {
    char* newStr = new char[strlen(source) + 1];
    strcpy(newStr, source);

    return newStr;
}

char* mergeStrings(const char* first, const char* second) {
    int lengthFirst = strlen(first);
    int lengthSecond = strlen(second);
    char* mergedString = new char[lengthFirst + lengthSecond + 1];
    strcpy(mergedString, first);
    strcpy(mergedString + lengthFirst, second);

    return mergedString;
}

/*
    Тест 1:
        Вход:
            c++ is better
            than ruby
        Изход:
            c++ is better than rudy
*/
