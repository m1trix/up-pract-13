/*
   Условие:
   Да се напише програма, в която потребителят въвежда положително число n.
   След това въвежда координатите на n на брой точки от равнината и извежда
   на екрана разстоянието между тях.
   Реализирайте чрез Питагорова Теорема.
   Използвайте отделна функция.
   */

/*
   Пример:
   Point1 x = 0
   Point1 y = 0
   Point2 x = 1
   Point2 y = 1
Distance: 1.41421
*/

/*
   Критерии за оценяване:
   1 - Кодът се компилира успешно.
   3 - Въвеждане на стойностите.
   5 - Създаване на функция, намираща разстоянието.
   1 - Извеждане на резултата.
   */

#include <iostream>
#include <math.h>
using namespace std;


double pointDistance(double, double, double, double);
void readDistances(int numPoints, double *pPointsX, double *pPointsY);
void calculateDistances(int numPoints,double *pPointsX, double *pPointsY);

int main() {
    int numPoints = 0;
    cout<<"Enter a number of points you want to enter: "<<endl;
    cin>>numPoints;
    // ===================================================
    // GETTING THE DISTANCE AND PRINTING IT TO THE SCREEN:
    // ===================================================
    if (numPoints > 1){
        double *pointsX = new double[numPoints];
        double *pointsY = new double[numPoints];
        readDistances(numPoints,pointsX,pointsY);
        calculateDistances(numPoints,pointsX,pointsY);
    } else {
        cout<<"Not enough points!"<<endl;
    }
    return 0;
}

void readDistances(int numberOfPoints, double *pPointsX, double *pPointsY){
    for(int i = 0; i < numberOfPoints; ++i) {
        cout << "Point" << i+1 << " x= ";
        cin >> pPointsX[i];
        cout << "Point" << i+1 << " y= ";
        cin >> pPointsY[i];
    }
}

void calculateDistances(int numPoints,double *pPointsX, double *pPointsY){
    for (int i = 0; i < numPoints - 1; ++i){
        cout<<"Distances between point "<<i+1<<" and "<<i+2<<" is: "<<
            pointDistance(pPointsX[i],pPointsY[i], pPointsX[i+1], pPointsY[i+1])<<endl;;
    }
}

double pointDistance(double px1, double py1, double px2, double py2) {
    return sqrt((px1 - px2)*(px1 - px2) + (py1 - py2)*(py1 - py2));
}

/*
   Тест 1:
   Вход:
   -1.1 -2.2 -3.3 -4.4
   Изход:
Distance: 3.11127
*/
