/*
    Условие:
        Напишете програма, която получава от потребителя цяло число, по-голямо от 1.
    Програмата трябва да задели двумерен целочислен масив, с размери равни на въведеното
    число. Въведеният масив трябва да има точно толкова елементи, колкото се използват.
        След като елементите на масива се въведат в програмата, той трябва да сеизведе
    на екрана, само че трябва да е обърнат огледално спрямо главния си диагонал,
	т.е. да е транспониран.
        На екрана трябва да се изведе новополученият масив.
        Нека има контрол на входа.
*/

/*
    Пример:
        Input size: 3
        Input array:
        1 2 3
        4 5 6
        7 8 9
        Result:
        1 4 7
        2 5 8
        3 6 9
*/

/*
    Критерии за оценяване:
        1 - Кодът се компилира успешно.
        1 - Въвеждане на число.
        1 - Контрол на входа.
        2 - Правилно заделяне на двумерен динамичен масив.
        1 - Въвеждане елементите на масива.
        2 - Транспониране.
        1 - Извеждане транспонирания масив на екрана.
        1 - Изтриване на динамична памет.
*/

#include <iostream>
using namespace std;


const int MIN_SIZE = 1;


int** create2DArray(int);
void delete2DArray(int**, int);

void input2DArray(int**, int);
void output2DArray(int**, int);

void transpose(int**, int);


int main() {
    // ====================
    // INPITUNG ARRAY SIZE:
    // ====================
    int size = 0;

    do {
        cout << "Input size: ";
        cin >> size;
    } while(size < MIN_SIZE);

    // ===================
    // INPUTING THE ARRAY:
    // ===================
    int** matrix;
    matrix = create2DArray(size);
    cout << "Input array:" << endl;
    input2DArray(matrix, size);

    // ==========================
    // TRANSPOSING AND OUTPUTING:
    // ==========================
    transpose(matrix, size);
    cout << "Result:" << endl;
    output2DArray(matrix, size);

    delete2DArray(matrix, size);

    return 0;
}

int** create2DArray(int size) {
    int** array = new int*[size];
    for(int i = 0; i < size; ++i)
        array[i] = new int[size];

    return array;
}

void delete2DArray(int** array, int size) {
    for(int i = 0; i < size; ++i)
        delete[] array[i];
    delete[] array;
}


void input2DArray(int** array, int size) {
    for(int i = 0; i < size; ++i)
        for(int j = 0; j < size; ++j)
            cin >> array[i][j];
}

void output2DArray(int** array, int size) {
    for(int i = 0; i < size; ++i) {
        for(int j = 0; j < size; ++j)
            cout << array[i][j] << " ";
        cout << endl;
    }
}


void transpose(int** array, int size) {
    for(int i = 0; i < size; ++i)
        for(int j = 0; j < i; ++j)
            swap(array[i][j], array[j][i]);
}

/*
    Тест 1:
        Вход:
            10
            1 2 3 4 5 6 7 8 9 0
            1 2 3 4 5 6 7 8 9 0
            1 2 3 4 5 6 7 8 9 0
            1 2 3 4 5 6 7 8 9 0
            1 2 3 4 5 6 7 8 9 0
            1 2 3 4 5 6 7 8 9 0
            1 2 3 4 5 6 7 8 9 0
            1 2 3 4 5 6 7 8 9 0
            1 2 3 4 5 6 7 8 9 0
            1 2 3 4 5 6 7 8 9 0
        Изход:
            1 1 1 1 1 1 1 1 1 1
            2 2 2 2 2 2 2 2 2 2
            3 3 3 3 3 3 3 3 3 3
            4 4 4 4 4 4 4 4 4 4
            5 5 5 5 5 5 5 5 5 5
            6 6 6 6 6 6 6 6 6 6
            7 7 7 7 7 7 7 7 7 7
            8 8 8 8 8 8 8 8 8 8
            9 9 9 9 9 9 9 9 9 9
            0 0 0 0 0 0 0 0 0 0
*/
