/*
    Условие:
        Напишете програма, която въвежда от стандартния вход две числа - размерите на двумерен
    целочислен масив. След това прочита всеки един от елементите на масива въведени от
    стандартния вход от потребителя. Програмaта трябва да изведе сумите на числата от всеки ред
    и стълб на масива, както и общатата сума на всички елементи. Препоръчително е да има контрол на входа.
*/

/*
 * Пример:
 * Input width: 3
 * Input height: 3
 * 1 2 3
 * 4 5 6
 * 7 8 9
 * Row 0: 6
 * Row 1: 15
 * Row 2: 24
 * Column 0: 12
 * Column 1: 15
 * Column 2: 18
 * All: 45
 * */

/*
 * Критерии за оценяване:
 * 1 - Кодът се компилира успешно.
 * 1 - Въвеждане на размерите на масива.
 * 2 - Въвеждане на стойностите на всеки елемент от масива.
 * 3 - Пресмятане на всички суми.
 * 1 - Извеждане на резултатите на екрана.
 * 2 - Контрол на входа.
 * */

#include <iostream>
using namespace std;


const int MAX_ARRAY_SIZE = 50;
const int MIN_ARRAY_SIZE = 1;


int main() {
    // ==========================
    // INPUTING WIDTH AND HEIGHT:
    // ==========================
    int width = 0, height = 0;

    do {
        cout << "Input array width [1, 50]: ";
        cin >> width;
    } while(width < MIN_ARRAY_SIZE || width > MAX_ARRAY_SIZE);

    do {
        cout << "Input array height [1, 50]: ";
        cin >> height;
    } while(height < MIN_ARRAY_SIZE || height > MAX_ARRAY_SIZE);

    // ============================================
    // INPUTING ARRAY VALUES AND COUNING ALL SUMMS:
    // ============================================
    int array[MAX_ARRAY_SIZE][MAX_ARRAY_SIZE];
    int summRow[MAX_ARRAY_SIZE] = {0};
    int summColumn[MAX_ARRAY_SIZE] = {0};
    int summAll = 0;

    for(int i = 0; i < height; ++i) {
        for(int j = 0; j < width; ++j) {
            cin >> array[i][j];
            summRow[i] += array[i][j];
            summColumn[j] += array[i][j];
        }
        summAll += summRow[i];
    }

    // ==========
    // OUTPUTING:
    // ==========
    for(int i = 0; i < height; ++i)
        cout << "Row " << i << ": " << summRow[i] << endl;

    for(int i = 0; i < width; ++i)
        cout << "Column " << i << ": " << summColumn[i] << endl;

    cout << "All: " << summAll << endl;

    return 0;
}

    /*
       Тест 1:
       Вход:
       3 3 1 1 1 1 1 1 1 1 1
       Изход:
       Row 0: 3
       Row 1: 3
       Row 2: 3
       Column 0: 3
       Column 1: 3
       Column 2: 3
       All: 9

       Tест 2:
        Вход:
        5 5 1 2 3 4 5 1 2 3 4 5 1 2 3 4 5 1 2 3 4 5 1 2 3 4 5
        Изход:
        Row 0: 15
        Row 1: 15
        Row 2: 15
        Row 3: 15
        Row 4: 15
        Column 0: 5
        Column 1: 10
        Column 2: 15
        Column 3: 20
        Column 4: 25
        All: 75

        Тест 3:
        Вход:
        1 1 1
        Изход:
        Row 0: 1
        Column 0: 1
        All: 1
    */
