/*
   Задача 1:
   */

/*
   Критерии за оценяване:
   */

#include <iostream>
#include <cstring>
using namespace std;

const int NUMBER_OF_ENGLISH_LETTERS = 26;
const char *keyMsg = "secret key", *textMsg = "text";

void getInput(char*& input, int& lenInput, const char* message);
void processInput (char* input, int length);
void removeWhiteSpace(char * str);
void toUpperCase(char* str);
char proccessLetter(char ch, char key, bool isDecryption);
void processMessage(char* text, const char* keyMask, bool isDecryption);
void chooseProcessingMethod(char* message, const char* keyMask);

void deleteArray(char*& arr);

int main() {
    int lenText = 0, lenKey = 0;
    char *key,*message;

    // ===============================================
    // INPUT AND PROCESS THE TEXT AND THE SECRET KEY:
    // ===============================================
    getInput(message,lenText,textMsg);
    getInput(key,lenKey,keyMsg);

    // =====================================
    // CHOOSE METHOD AND ENCODING/DECODING:
    // =====================================
    chooseProcessingMethod(message,key);

    // ======================
    // DISPLAY THE RESULT:
    // ======================
    cout<<"The processed message is: "<<endl;
    cout<<message<<endl;

    // =================
    // FREE THE MEMORY:
    // =================
    deleteArray(key);
    deleteArray(message);
    cout<<message<<endl;

    return 0;
}

void getInput(char*& input, int& lenInput, const char* message){
    cout<<"Enter the max length of "<<message<<" you want to enter: ";
    cin>>lenInput; cin.get();
    char* inputText = new char [++lenInput];
    cout<<"Enter the "<<message<<": ";
    processInput(inputText,lenInput);
    lenInput = strlen(inputText) + 1;
    input = new char [lenInput];
    strncpy(input,inputText,lenInput);
    delete [] inputText;
}

void processInput (char* input, int length){
    cin.getline(input,length);
    removeWhiteSpace(input);
    toUpperCase(input);
}

void removeWhiteSpace(char * str){
    int index = 0;
    for(unsigned i = 0; i < strlen(str); ++i)
        if(str[i] != ' '){
            str[index] = str[i];
            ++index;
        }
    str[index]= '\0';
}

void toUpperCase(char* str){
    while(*str){
        if(*str >= 'a' && *str <= 'z')
            *str -= 'a'-'A';
        ++str;
    }
}

char proccessLetter(char ch, char key, bool isDecryption){
    int numberOfLetter = ch - 'A';
    int numberOfKey    = key - 'A';
    if(isDecryption)
        numberOfKey = NUMBER_OF_ENGLISH_LETTERS - numberOfKey;
    int numberOfModified = (numberOfLetter + numberOfKey) % NUMBER_OF_ENGLISH_LETTERS;
    return 'A' + numberOfModified;
}

void processMessage(char* text, const char* key, bool isDecryption){
    for(unsigned i = 0; i < strlen(text); ++i)
        text[i] = proccessLetter(text[i],key[i % strlen(key)],isDecryption);
}

void chooseProcessingMethod(char* message, const char* keyMask){
    char option;

    do{
        cout<<"What do you want to do: E for encrypt, D for decrypt: ";
        cin>>option;
    }
    while (option != 'D' && option != 'E');

    bool isDecryption = false;
    if (option == 'D')
        isDecryption = true;

    processMessage(message,keyMask,isDecryption);
}

void deleteArray(char*& arr){
    delete [] arr;
    arr = 0;
}
/*   Тест 1:
     Нешифрован текст:
     ATTACKATDAWN

     Ключова повторена дума:
     LEMON

     Шифрован текст:
     LXFOPVEFRNHR

    Plaintext:  CRYPTOISSHORTFORCRYPTOGRAPHY
    Key:        ABCD
    Ciphertext: CSASTPKVSIQUTGQUCSASTPIUAQJB
*/

