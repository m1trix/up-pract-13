#include <iostream>
using namespace std;


void operationEncode();
void operationDecode();
char* readText();
void editText(char*);
bool isLetter(char);
void encodeText(char*, char);
void crackCode(char*);

int main() {

    char operation;
    cout << "Input the operation: ";
    cin >> operation;
    cout << endl;

    switch(operation) {

        case 'd':
        case 'D':
            operationDecode();
            break;

        case 'e':
        case 'E':
            operationEncode();
            break;

        default:
            cout << "Unknown operation!" << endl;
    }

    return 0;
}

char* readText() {

    int textLength;
    cout << "Input maximal text length: ";
    cin >> textLength;
    cin.get();

    char* text = new char[textLength];
    cout << "Input the text: ";
    cin.getline(text, textLength);

    return text;
}

bool isLetter(char symbol) {

    return (symbol >= 'A' && symbol <= 'Z') || (symbol >= 'a' && symbol <= 'z');
}

void editText(char* text) {

    int index = 0;
    for(int i = 0; text[i] != '\0'; ++i) {
        if(isLetter(text[i])) {
            if(text[i] > 'Z')
                text[i] -= 'a' - 'A';
            text[index++] = text[i];
        }
    }
    text[index] = '\0';
}

void operationEncode() {

    // ====================
    //  READING USER DATA:
    // ====================
    char* text = readText();
    char key;
    cout << "Input the coding letter: ";
    cin >> key;

    if(!isLetter(key)) {
        cout << "Invalid sumbol!" << endl;
        return;
    }

    // =====================
    //  ENCODOING THE TEXT:
    // =====================
    if(key > 'Z')
        key -= 'a' - 'A';


    editText(text);
    encodeText(text, key);

    cout << endl << "Result: " << text << endl;

    delete[] text;

}

void operationDecode() {

    char* text = readText();

    editText(text);
    crackCode(text);

    delete[] text;

}

void encodeText(char* text, char key) {

    int shift = key - 'A'; // if key == 'A', shift = 0, nothing changes

    for(int i = 0; text[i] != '\0'; ++i) {

        int letter = text[i] - 'A';
        letter = (shift + letter) % 26;
        text[i] = letter + 'A';
    }
}

void crackCode(char* text) {

    for(char letter = 'B'; letter <= 'Z'; ++letter) {
        encodeText(text, 'Z'); // shifts left with one step
        cout << "Key \'" << letter << "\': " << text << endl << endl;
    }

    encodeText(text, 'Z'); // back to normal
}
