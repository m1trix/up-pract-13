/*
    Условие:
        Напишете програма, която изчислява и извежда на екрана стойността неперовото число.
        Използвайте 10 стъпки, за да няма неверни стойности на факториел.
*/

/*
    Пример:
    е = 2.71828
*/

/*
    Критерии за оценяване:
    1 - Кодът се компилира успешно.
    4 - Изчисляване на неперовото число.
    4 - Изчисляване на факториел.
    1 - ИЗвеждане резултат на екрана.
*/

#include <iostream>
using namespace std;


const int STEPS = 10;


double EulersNumber(int presiciton);

int main() {
    double neperNumber = 1;
    double fact = 1;

    for(int i = 1; i < STEPS; ++i) {
        fact *= i;
        neperNumber += 1 / fact;
    }

    cout << "e = " << neperNumber << endl;

    return 0;
}


