CXX = g++
CXXFLAGS=-Wall -std=c++11
PROG = app
INC =
LIBS =  
SRCS = main.cpp

all: $(PROG)

$(PROG):	$(SRCS)
	$(CXX) $(CXXFLAGS) -o $(PROG) $(SRCS) $(INC) $(LIBS)

clean:
	rm -f $(PROG)
